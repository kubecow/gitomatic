FROM debian:buster-slim


RUN set -x && apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    --no-install-recommends \
    ca-certificates bash git curl curl gettext jq ssh \
    && rm -rf /var/lib/apt/lists/* 

RUN curl -sL https://github.com/muesli/gitomatic/releases/download/v0.2/gitomatic_0.2_Linux_x86_64.tar.gz | tar -xvz && \
    mv gitomatic /usr/bin/gitomatic && \
    chmod +x /usr/bin/gitomatic && \
    rm -rf gitomatic_0.2_Linux_x86_64.tar.gz LICENSE README.md


WORKDIR /
CMD bash
 